import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ImageCropperModule} from "ngx-image-cropper";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {NgxCameraModule} from "ngx-camera";
import {WebcamModule} from "ngx-webcam";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TessComponent } from './tess/tess.component';
import { TessertComponent } from './tessert/tessert.component';
import {RouterModule} from "@angular/router";
import { TesseractComponent } from './tesseract/tesseract.component';

@NgModule({
  declarations: [
    AppComponent,
    TessComponent,
    TessertComponent,
    TesseractComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ImageCropperModule,
    CommonModule,
    FormsModule,
    WebcamModule,
    BrowserAnimationsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
