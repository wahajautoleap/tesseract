import { Component, OnInit } from '@angular/core';
import {ImageCroppedEvent} from "ngx-image-cropper";
import {createWorker} from "tesseract.js";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Observable, Subject} from "rxjs";
import {WebcamImage} from "ngx-webcam";

@Component({
  selector: 'app-tesseract',
  templateUrl: './tesseract.component.html',
  styleUrls: ['./tesseract.component.scss']
})
export class TesseractComponent implements OnInit {

  worker: Tesseract.Worker = createWorker();
  public webcamImage: WebcamImage = null;
  isReady: boolean;
  base64Image: any;
  imageChangedEvent: any;
  croppedImage: any;
  isScanning: boolean;
  ocrResult: any;
  private trigger: Subject<void> = new Subject<void>();


  constructor(public modalService:NgbModal) { }

  ngOnInit(): void {
  }

  imageCropped(event: ImageCroppedEvent): void {
    console.log(event);
    //this.doOCR(event.base64);
    this.croppedImage = event.base64;
    this.base64Image = event.base64;
  }

  scanOCR() {
    this.isScanning = true;
    this.imageChangedEvent = null;
    this.doOCR(this.croppedImage);
  }

  async doOCR(base64Image: string) {
    this.ocrResult = "Scanning";
    console.log(`Started: ${new Date()}`);
    if (this.isReady) {
      const data = await this.worker.recognize(base64Image);
      console.log(data);
      this.ocrResult = data.data.text;
    }
    // await this.worker.terminate();
    console.log(`Stopped: ${new Date()}`);
    this.isScanning = false;
  }

  triggerSnapshot(): void {
    this.trigger.next();
    this.fileChangeEvent(this.base64Image);
    console.log(this.base64Image)
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.base64Image = event;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  handleImage(webcamImage: WebcamImage): void {
    console.info('Saved webcam image', webcamImage);
    this.webcamImage = webcamImage;
    this.base64Image = webcamImage.imageAsDataUrl
    this.imageChangedEvent = webcamImage.imageAsDataUrl
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    }, (reason) => {
    });
  }
}
