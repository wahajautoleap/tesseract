import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TessertComponent } from './tessert.component';

describe('TessertComponent', () => {
  let component: TessertComponent;
  let fixture: ComponentFixture<TessertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TessertComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TessertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
