import { Component } from '@angular/core';
import {VERSION} from "@angular/cli";
import { createWorker } from "tesseract.js";
import * as Tesseract from "tesseract.js";
import {ImageCroppedEvent} from "ngx-image-cropper";
import {WebcamImage} from "ngx-webcam";
import {Observable, Subject} from "rxjs";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  worker: Tesseract.Worker = createWorker();
  isReady: boolean;
  imageChangedEvent: any;
  base64Image: any;
  ocrResult: string;
  croppedImage: any = "";
  isScanning: boolean;
  public webcamImage: WebcamImage = null;
  private trigger: Subject<void> = new Subject<void>();

  constructor(private modalService: NgbModal) {
    this.initialize();
  }
  async initialize(): Promise<void> {
    await this.worker.load();
    await this.worker.loadLanguage("eng");
    await this.worker.initialize("eng");
    this.isReady = true;
  }
  handleFileInput(event): void {
    //  console.log(event);

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.imageChangedEvent = event;

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event: any) => {
        this.base64Image = event.target.result;
        event.target.result = null;
      };
    }
  }

  scanOCR() {
    this.isScanning = true;
    this.imageChangedEvent = null;
    this.doOCR(this.croppedImage);
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.base64Image = event;
  }
  imageCropped(event: ImageCroppedEvent ): void {
    console.log(event);
    //this.doOCR(event.base64);
    this.croppedImage = event.base64;
    this.base64Image = event.base64;
  }

  async doOCR(base64Image: string) {
    this.ocrResult = "Scanning";
    console.log(`Started: ${new Date()}`);
    if (this.isReady) {
      const data = await this.worker.recognize(base64Image);
      console.log(data);
      this.ocrResult = data.data.text;
    }
    // await this.worker.terminate();
    console.log(`Stopped: ${new Date()}`);
    this.isScanning = false;
  }

  transform(): string {
    return this.base64Image;
  }


  triggerSnapshot(): void {
    this.trigger.next();
    this.fileChangeEvent('capture');
  }
  handleImage(webcamImage: WebcamImage): void {
    console.info('Saved webcam image', webcamImage);
    this.webcamImage = webcamImage;
    this.base64Image = webcamImage.imageAsBase64
    this.imageChangedEvent = webcamImage.imageAsDataUrl
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    }, (reason) => {
    });
  }
}
