import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TessComponent} from "./tess/tess.component";
import {AppComponent} from "./app.component";
import {TessertComponent} from "./tessert/tessert.component";
import {TesseractComponent} from "./tesseract/tesseract.component";

const routes: Routes = [{path: 'tess', component: TessComponent}, {
  path: 'tesser',
  component: TessertComponent
}, {path: 'tesseract', component: TesseractComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
